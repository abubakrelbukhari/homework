from nose.tools import assert_equal
import aims
def test_positive():
    list = [3, 3]
    abss = aims.std(list)
    exp = 0
    assert_equal(abss ,exp)
def test_negative():
    list = [-3 ,-4]
    abss = aims.std(list)
    exp = 0.35355339059327379
    assert_equal(abss ,exp)
def test_float():
    list = [0.2, 0.4]
    abss = aims.std(list)
    exp =0.070710678118654738
    assert_equal(abss,exp)
